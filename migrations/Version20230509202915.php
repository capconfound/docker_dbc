<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230509202915 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE appointment CHANGE client_id client_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE appointment ADD CONSTRAINT FK_FE38F84419EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE appointment ADD CONSTRAINT FK_FE38F844783E3463 FOREIGN KEY (manager_id) REFERENCES employee (id)');
        $this->addSql('CREATE INDEX IDX_FE38F84419EB6921 ON appointment (client_id)');
        $this->addSql('CREATE INDEX IDX_FE38F844783E3463 ON appointment (manager_id)');
        $this->addSql('ALTER TABLE client_car ADD CONSTRAINT FK_5099753919EB6921 FOREIGN KEY (client_id) REFERENCES client (id)');
        $this->addSql('ALTER TABLE client_car ADD CONSTRAINT FK_50997539C3C6F69F FOREIGN KEY (car_id) REFERENCES car (id)');
        $this->addSql('CREATE INDEX IDX_5099753919EB6921 ON client_car (client_id)');
        $this->addSql('CREATE INDEX IDX_50997539C3C6F69F ON client_car (car_id)');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F88C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id)');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F818229D0A FOREIGN KEY (malfunction_id) REFERENCES malfunction (id)');
        $this->addSql('ALTER TABLE job ADD CONSTRAINT FK_FBD8E0F8E5B533F9 FOREIGN KEY (appointment_id) REFERENCES appointment (id)');
        $this->addSql('CREATE INDEX IDX_FBD8E0F88C03F15C ON job (employee_id)');
        $this->addSql('CREATE INDEX IDX_FBD8E0F818229D0A ON job (malfunction_id)');
        $this->addSql('CREATE INDEX IDX_FBD8E0F8E5B533F9 ON job (appointment_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F88C03F15C');
        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F818229D0A');
        $this->addSql('ALTER TABLE job DROP FOREIGN KEY FK_FBD8E0F8E5B533F9');
        $this->addSql('DROP INDEX IDX_FBD8E0F88C03F15C ON job');
        $this->addSql('DROP INDEX IDX_FBD8E0F818229D0A ON job');
        $this->addSql('DROP INDEX IDX_FBD8E0F8E5B533F9 ON job');
        $this->addSql('ALTER TABLE client_car DROP FOREIGN KEY FK_5099753919EB6921');
        $this->addSql('ALTER TABLE client_car DROP FOREIGN KEY FK_50997539C3C6F69F');
        $this->addSql('DROP INDEX IDX_5099753919EB6921 ON client_car');
        $this->addSql('DROP INDEX IDX_50997539C3C6F69F ON client_car');
        $this->addSql('ALTER TABLE appointment DROP FOREIGN KEY FK_FE38F84419EB6921');
        $this->addSql('ALTER TABLE appointment DROP FOREIGN KEY FK_FE38F844783E3463');
        $this->addSql('DROP INDEX IDX_FE38F84419EB6921 ON appointment');
        $this->addSql('DROP INDEX IDX_FE38F844783E3463 ON appointment');
        $this->addSql('ALTER TABLE appointment CHANGE client_id client_id INT NOT NULL');
    }
}
