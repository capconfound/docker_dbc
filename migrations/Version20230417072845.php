<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230417072845 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE job_part (job_id INT NOT NULL, part_id INT NOT NULL, INDEX IDX_CEC30687BE04EA9 (job_id), INDEX IDX_CEC306874CE34BEC (part_id), PRIMARY KEY(job_id, part_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE job_part ADD CONSTRAINT FK_CEC30687BE04EA9 FOREIGN KEY (job_id) REFERENCES job (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE job_part ADD CONSTRAINT FK_CEC306874CE34BEC FOREIGN KEY (part_id) REFERENCES part (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job_part DROP FOREIGN KEY FK_CEC30687BE04EA9');
        $this->addSql('ALTER TABLE job_part DROP FOREIGN KEY FK_CEC306874CE34BEC');
        $this->addSql('DROP TABLE job_part');
    }
}
