<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230417064957 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE car_part ADD CONSTRAINT FK_8265646AC3C6F69F FOREIGN KEY (car_id) REFERENCES car (id)');
        $this->addSql('ALTER TABLE car_part ADD CONSTRAINT FK_8265646A4CE34BEC FOREIGN KEY (part_id) REFERENCES part (id)');
        $this->addSql('CREATE INDEX IDX_8265646AC3C6F69F ON car_part (car_id)');
        $this->addSql('CREATE INDEX IDX_8265646A4CE34BEC ON car_part (part_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE car_part DROP FOREIGN KEY FK_8265646AC3C6F69F');
        $this->addSql('ALTER TABLE car_part DROP FOREIGN KEY FK_8265646A4CE34BEC');
        $this->addSql('DROP INDEX IDX_8265646AC3C6F69F ON car_part');
        $this->addSql('DROP INDEX IDX_8265646A4CE34BEC ON car_part');
    }
}
