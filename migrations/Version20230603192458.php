<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230603192458 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE job ADD time_taken_in_minutes INT NOT NULL, CHANGE appointment_id appointment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE part ADD sku VARCHAR(50) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE part DROP sku');
        $this->addSql('ALTER TABLE job DROP time_taken_in_minutes, CHANGE appointment_id appointment_id INT NOT NULL');
    }
}
