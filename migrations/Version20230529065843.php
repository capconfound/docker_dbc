<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230529065843 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE appointment ADD client_car_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE appointment ADD CONSTRAINT FK_FE38F8449660BD2C FOREIGN KEY (client_car_id) REFERENCES client_car (id)');
        $this->addSql('CREATE INDEX IDX_FE38F8449660BD2C ON appointment (client_car_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE appointment DROP FOREIGN KEY FK_FE38F8449660BD2C');
        $this->addSql('DROP INDEX IDX_FE38F8449660BD2C ON appointment');
        $this->addSql('ALTER TABLE appointment DROP client_car_id');
    }
}
