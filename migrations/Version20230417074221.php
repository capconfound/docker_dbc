<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230417074221 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE part_supplier (part_id INT NOT NULL, supplier_id INT NOT NULL, INDEX IDX_C78E9014CE34BEC (part_id), INDEX IDX_C78E9012ADD6D8C (supplier_id), PRIMARY KEY(part_id, supplier_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE part_supplier ADD CONSTRAINT FK_C78E9014CE34BEC FOREIGN KEY (part_id) REFERENCES part (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE part_supplier ADD CONSTRAINT FK_C78E9012ADD6D8C FOREIGN KEY (supplier_id) REFERENCES supplier (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE part_supplier DROP FOREIGN KEY FK_C78E9014CE34BEC');
        $this->addSql('ALTER TABLE part_supplier DROP FOREIGN KEY FK_C78E9012ADD6D8C');
        $this->addSql('DROP TABLE part_supplier');
    }
}
