<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ClientRepository::class)]
class Client
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column(length: 255)]
    private ?string $phone = null;

    #[ORM\OneToMany(mappedBy: 'client', targetEntity: ClientCar::class, orphanRemoval: true)]
    private Collection $clientCars;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    public function __construct()
    {
        $this->clientCars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection<int, ClientCar>
     */
    public function getClientCars(): Collection
    {
        return $this->clientCars;
    }

    public function getClientCarsLabels(): string
    {
        $clientCarLabels = [];
        $clientCars = $this->getClientCars();

        foreach ($clientCars as $clientCar) {
            $clientCarLabels[] = $clientCar->getCar()->getName();
        }
        return implode(', ', $clientCarLabels);
    }

    public function addClientCar(ClientCar $clientCar): self
    {
        if (!$this->clientCars->contains($clientCar)) {
            $this->clientCars->add($clientCar);
            $clientCar->setClient($this);
        }

        return $this;
    }

    public function removeClientCar(ClientCar $clientCar): self
    {
        if ($this->clientCars->removeElement($clientCar)) {
            // set the owning side to null (unless already changed)
            if ($clientCar->getClient() === $this) {
                $clientCar->setClient(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->getId().' '.$this->getName();
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
