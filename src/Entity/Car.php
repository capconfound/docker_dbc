<?php

namespace App\Entity;

use App\Repository\CarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CarRepository::class)]
class Car
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $make = null;

    #[ORM\Column(length: 255)]
    private ?string $model = null;

    #[ORM\OneToMany(mappedBy: 'car', targetEntity: CarPart::class)]
    private Collection $carParts;

    public function __construct()
    {
        $this->carParts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMake(): ?string
    {
        return $this->make;
    }

    public function setMake(string $make): self
    {
        $this->make = $make;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return Collection<int, CarPart>
     */
    public function getCarParts(): Collection
    {
        return $this->carParts;
    }

    public function addCarPart(CarPart $carPart): self
    {
        if (!$this->carParts->contains($carPart)) {
            $this->carParts->add($carPart);
            $carPart->setCar($this);
        }

        return $this;
    }

    public function removeCarPart(CarPart $carPart): self
    {
        if ($this->carParts->removeElement($carPart)) {
            // set the owning side to null (unless already changed)
            if ($carPart->getCar() === $this) {
                $carPart->setCar(null);
            }
        }

        return $this;
    }

    public function getName(): string
    {
        return $this->getMake() . ' ' . $this->getModel();
    }
}
