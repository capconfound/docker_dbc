<?php

namespace App\Entity;

use App\Repository\CarPartRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CarPartRepository::class)]
class CarPart
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?int $carId = null;

    #[ORM\Column]
    private ?int $partId = null;

    #[ORM\ManyToOne(inversedBy: 'carParts')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Car $car = null;

    #[ORM\ManyToOne(inversedBy: 'carParts')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Part $part = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCarId(): ?int
    {
        return $this->carId;
    }

    public function setCarId(int $carId): self
    {
        $this->carId = $carId;

        return $this;
    }

    public function getPartId(): ?int
    {
        return $this->partId;
    }

    public function setPartId(int $partId): self
    {
        $this->partId = $partId;

        return $this;
    }

    public function getCar(): ?Car
    {
        return $this->car;
    }

    public function setCar(?Car $car): self
    {
        $this->car = $car;

        return $this;
    }

    public function getPart(): ?Part
    {
        return $this->part;
    }

    public function setPart(?Part $part): self
    {
        $this->part = $part;

        return $this;
    }
}
