<?php

namespace App\Entity;

use App\Repository\PartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PartRepository::class)]
class Part
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\Column]
    private ?int $type = null;

    #[ORM\Column]
    private array $suppliers = [];

    #[ORM\Column]
    private array $carId = [];

    #[ORM\OneToMany(mappedBy: 'part', targetEntity: CarPart::class)]
    private Collection $carParts;

    #[ORM\ManyToMany(targetEntity: Supplier::class, inversedBy: 'parts')]
    private Collection $supplier;

    #[ORM\Column(length: 50, nullable: true)]
    private ?string $sku = null;

    #[ORM\Column(nullable: true)]
    private ?float $price = null;

    public function __construct()
    {
        $this->carParts = new ArrayCollection();
        $this->supplier = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSuppliers(): array
    {
        return $this->suppliers;
    }

    public function setSuppliers(array $suppliers): self
    {
        $this->suppliers = $suppliers;

        return $this;
    }

    public function getCarId(): array
    {
        return $this->carId;
    }

    public function setCarId(array $carId): self
    {
        $this->carId = $carId;

        return $this;
    }

    /**
     * @return Collection<int, CarPart>
     */
    public function getCarParts(): Collection
    {
        return $this->carParts;
    }

    public function addCarPart(CarPart $carPart): self
    {
        if (!$this->carParts->contains($carPart)) {
            $this->carParts->add($carPart);
            $carPart->setPart($this);
        }

        return $this;
    }

    public function removeCarPart(CarPart $carPart): self
    {
        if ($this->carParts->removeElement($carPart)) {
            // set the owning side to null (unless already changed)
            if ($carPart->getPart() === $this) {
                $carPart->setPart(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Supplier>
     */
    public function getSupplier(): Collection
    {
        return $this->supplier;
    }

    public function addSupplier(Supplier $supplier): self
    {
        if (!$this->supplier->contains($supplier)) {
            $this->supplier->add($supplier);
        }

        return $this;
    }

    public function removeSupplier(Supplier $supplier): self
    {
        $this->supplier->removeElement($supplier);

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(?string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
