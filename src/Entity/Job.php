<?php

namespace App\Entity;

use App\Repository\JobRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: JobRepository::class)]
class Job
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $employeeId = null;

    #[ORM\Column]
    private array $parts = [];

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $startedAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $finishedAt = null;

    #[ORM\ManyToMany(targetEntity: Part::class)]
    private Collection $part;

    #[ORM\ManyToOne]
    private ?Employee $employee = null;

    #[ORM\ManyToOne]
    private ?Malfunction $malfunction = null;

    #[ORM\ManyToOne(inversedBy: 'jobs')]
    private ?Appointment $appointment = null;

    #[ORM\Column]
    private ?int $timeTakenInMinutes = null;

    #[ORM\ManyToOne]
    private ?Service $service = null;

    public function __construct()
    {
        $this->part = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMalfunctionId(): ?int
    {
        return $this->malfunctionId;
    }

    public function setMalfunctionId(?int $malfunctionId): self
    {
        $this->malfunctionId = $malfunctionId;

        return $this;
    }

    public function getEmployeeId(): ?int
    {
        return $this->employeeId;
    }

    public function setEmployeeId(?int $employeeId): self
    {
        $this->employeeId = $employeeId;

        return $this;
    }

    public function getAppointmentId(): ?int
    {
        return $this->appointmentId;
    }

    public function setAppointmentId(int $appointmentId): self
    {
        $this->appointmentId = $appointmentId;

        return $this;
    }

    public function getParts(): array
    {
        return $this->parts;
    }

    public function setParts(array $parts): self
    {
        $this->parts = $parts;

        return $this;
    }

    public function getStartedAt(): ?\DateTimeImmutable
    {
        return $this->startedAt;
    }

    public function setStartedAt(\DateTimeImmutable $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getFinishedAt(): ?\DateTimeImmutable
    {
        return $this->finishedAt;
    }

    public function setFinishedAt(?\DateTimeImmutable $finishedAt): self
    {
        $this->finishedAt = $finishedAt;

        return $this;
    }

    /**
     * @return Collection<int, Part>
     */
    public function getPart(): Collection
    {
        return $this->part;
    }

    public function addPart(Part $part): self
    {
        if (!$this->part->contains($part)) {
            $this->part->add($part);
        }

        return $this;
    }

    public function removePart(Part $part): self
    {
        $this->part->removeElement($part);

        return $this;
    }

    public function getEmployee(): ?Employee
    {
        return $this->employee;
    }

    public function setEmployee(?Employee $employee): self
    {
        $this->employee = $employee;

        return $this;
    }

    public function getMalfunction(): ?Malfunction
    {
        return $this->malfunction;
    }

    public function setMalfunction(?Malfunction $malfunction): self
    {
        $this->malfunction = $malfunction;

        return $this;
    }

    public function getAppointment(): ?Appointment
    {
        return $this->appointment;
    }

    public function setAppointment(?Appointment $appointment): self
    {
        $this->appointment = $appointment;

        return $this;
    }

    public function getTimeTakenInMinutes(): ?int
    {
        return $this->timeTakenInMinutes;
    }

    public function setTimeTakenInMinutes(int $timeTakenInMinutes): self
    {
        $this->timeTakenInMinutes = $timeTakenInMinutes;

        return $this;
    }

    public function getService(): ?Service
    {
        return $this->service;
    }

    public function setService(?Service $service): self
    {
        $this->service = $service;

        return $this;
    }
}
