<?php

namespace App\Entity;

use App\Repository\AppointmentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AppointmentRepository::class)]
class Appointment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'appointments')]
    private ?Client $client = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $date = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $complaints = null;

    #[ORM\Column(nullable: true)]
    private ?int $status = null;


    #[ORM\ManyToOne]
    private ?Employee $manager = null;

    #[ORM\OneToMany(mappedBy: 'appointment', targetEntity: Job::class)]
    private Collection $jobs;

    #[ORM\ManyToOne(inversedBy: 'appointment', targetEntity: ClientCar::class)]
    #[ORM\JoinColumn(name: 'client_car_id', referencedColumnName: 'id')]
    private ?ClientCar $clientCar = null;

    #[ORM\ManyToMany(targetEntity: Part::class)]
    private Collection $parts;

    public function __construct()
    {
        $this->jobs = new ArrayCollection();
        $this->parts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(\DateTimeImmutable $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getComplaints(): ?string
    {
        return $this->complaints;
    }

    public function setComplaints(?string $complaints): self
    {
        $this->complaints = $complaints;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getClient(): ?Client
    {
        return $this->client;
    }

    public function setClient(?Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getManager(): ?Employee
    {
        return $this->manager;
    }

    public function setManager(?Employee $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * @return Collection<int, Job>
     */
    public function getJobs(): Collection
    {
        return $this->jobs;
    }

    public function addJob(Job $job): self
    {
        if (!$this->jobs->contains($job)) {
            $this->jobs->add($job);
            $job->setAppointment($this);
        }

        return $this;
    }

    public function removeJob(Job $job): self
    {
        if ($this->jobs->removeElement($job)) {
            // set the owning side to null (unless already changed)
            if ($job->getAppointment() === $this) {
                $job->setAppointment(null);
            }
        }

        return $this;
    }

    public function getClientName(): string
    {
        return $this->client.'';
    }
    public function getClientPhone(): string
    {
        return $this->client->getPhone();
    }

    public function getManagerName(): string
    {
        return $this->manager.'';
    }

    public function getFormattedDate(): string
    {
        return $this->getDate()->format('Y-m-d H:i');
    }

    public function getClientCar(): ?ClientCar
    {
        return $this->clientCar;
    }

    public function setClientCar(?ClientCar $clientCar): self
    {
        $this->clientCar = $clientCar;

        return $this;
    }

    /**
     * @return Collection<int, Part>
     */
    public function getParts(): Collection
    {
        return $this->parts;
    }

    public function addPart(Part $part): self
    {
        if (!$this->parts->contains($part)) {
            $this->parts->add($part);
        }

        return $this;
    }

    public function removePart(Part $part): self
    {
        $this->parts->removeElement($part);

        return $this;
    }
}
