<?php

namespace App\Form;

use App\Entity\Appointment;
use App\Repository\ClientRepository;
use App\Repository\EmployeeRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AppointmentType extends AbstractType
{
    public function __construct(
        private ClientRepository $clientRepository,
        private EmployeeRepository $employeeRepository
    )
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $clients = $this->clientRepository->findAll();
        $employees = $this->employeeRepository->findAll();
        $clientCars = [];

        $builder
            ->add('date', DateTimeType::class, [
                'input' => 'datetime_immutable',
                'input_format' => 'Y-m-d H:i',
                'widget' => 'single_text',
                'data' => new \DateTimeImmutable(),

            ])
            ->add('complaints')
            ->add('client', ChoiceType::class, [
                'choices' => $clients,
                'choice_value' => 'id',
                'choice_label' => 'name',
            ])
            ->add('manager', ChoiceType::class, [
                'choices' => $employees,
                'choice_value' => 'id',
                'choice_label' => 'name',
            ])
            ->add('clientCar', ChoiceType::class, [
                'choices' => $clientCars,
                'choice_value' => 'id',
                'choice_label' => 'name',
            ])
            ->add('submit', SubmitType::class, ['label' => 'Добавить'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Appointment::class,
            'data' => new \DateTimeImmutable(),
        ]);
    }
}
