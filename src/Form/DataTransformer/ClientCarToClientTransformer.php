<?php

namespace App\Form\DataTransformer;

use App\Entity\ClientCar;
use Symfony\Component\Form\DataTransformerInterface;

class ClientCarToClientTransformer implements DataTransformerInterface
{
    public function transform($value)
    {
        if (null === $value) {
            return null;
        }

        return $value->getClient();
    }

    public function reverseTransform($value)
    {
        if (null === $value) {
            return null;
        }

        $clientCar = new ClientCar();
        $clientCar->setClient($value);

        return $clientCar;
    }
}