<?php

namespace App\Form;

use App\Entity\Appointment;
use App\Entity\Car;
use App\Entity\Client;
use App\Entity\ClientCar;
use App\Repository\CarRepository;
use App\Repository\ClientRepository;
use App\Repository\EmployeeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    public function __construct(
        private CarRepository $carRepository,
    )
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
//        $cars = $this->carRepository->findAll();

        $builder
            ->add('name')
            ->add('phone')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Client::class,
        ]);
    }
}
