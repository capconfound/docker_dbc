<?php

namespace App\Form;

use App\Entity\Appointment;
use App\Entity\Car;
use App\Entity\Client;
use App\Entity\ClientCar;
use App\Form\DataTransformer\ClientCarToClientTransformer;
use App\Repository\CarRepository;
use App\Repository\ClientRepository;
use App\Repository\EmployeeRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientCarType extends AbstractType
{
    public function __construct(
        private ClientCarToClientTransformer $transformer
    )
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('vin', TextType::class)
            ->add('plate', TextType::class)
            ->add('client', EntityType::class, [
                'class' => Client::class,
                'choice_label' => 'name',
            ])//->addModelTransformer($this->transformer)
            ->add('car', EntityType::class, [
                'class' => Car::class,
                'choice_label' => 'name',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ClientCar::class,
        ]);
    }
}
