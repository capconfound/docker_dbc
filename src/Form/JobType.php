<?php

namespace App\Form;

use App\Entity\Appointment;
use App\Entity\Car;
use App\Entity\Client;
use App\Entity\ClientCar;
use App\Entity\Employee;
use App\Entity\Job;
use App\Entity\Malfunction;
use App\Entity\Part;
use App\Entity\Service;
use App\Repository\JobRepository;
use App\Repository\MalfunctionRepository;
use App\Repository\PartRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobType extends AbstractType
{

    public function __construct(
        private JobRepository $jobRepository,
        private MalfunctionRepository $malfunctionRepository,
        private PartRepository $partRepository,
    )
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
//        $builder

//            ->add('appointment', EntityType::class, [
//                'class' => Appointment::class,
//                'choice_label' => 'getFormattedDate',
//            ])
//            ->add('part', EntityType::class, [
//                'class' => Part::class,
//                'choice_label' => 'name',
//            ])
//            ->add('services', EntityType::class, [
//                'class' => Service::class,
//                'choice_label' => 'name'
//            ])
//        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Job::class,
        ]);
    }
}
