<?php

namespace App\Repository;

use App\Entity\ClientCar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ClientCar>
 *
 * @method ClientCar|null find($id, $lockMode = null, $lockVersion = null)
 * @method ClientCar|null findOneBy(array $criteria, array $orderBy = null)
 * @method ClientCar[]    findAll()
 * @method ClientCar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientCarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ClientCar::class);
    }

    public function save(ClientCar $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ClientCar $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getCarIdsByClientId(int $clientId): array
    {
        if (!$clientId) return [];

        return $this->createQueryBuilder('c')
            ->select('c.id')
            ->andWhere('c.client = :clientId')
            ->setParameter('clientId', $clientId)
            ->getQuery()
            ->getResult()
        ;

    }

//    /**
//     * @return ClientCar[] Returns an array of ClientCar objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ClientCar
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
