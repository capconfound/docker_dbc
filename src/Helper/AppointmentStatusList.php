<?php

namespace App\Helper;

class AppointmentStatusList
{
    public const SCHEDULED = 1;
    public const IN_PROGRESS = 2;
    public const READY = 3;
    public const FINISHED = 4;
    public const DELETED = 5;

    public function getLabels(): array
    {
        return [
            self::SCHEDULED => 'Создано',
            self::IN_PROGRESS => 'В работе',
            self::READY => 'Готово',
            self::FINISHED => 'Выдано клиенту',
            self::DELETED => 'Удалено',
        ];
    }

    public function getLabel(string $id): string
    {
        return $this->getLabels()[$id] ?? '';
    }
}