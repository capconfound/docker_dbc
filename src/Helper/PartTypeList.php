<?php

namespace App\Helper;

class PartTypeList
{
    public const PART_ICU = 1;
    public const PART_BODY = 2;
    public const PART_SUSPENSION = 3;
    public const PART_DRIVETRAIN = 4;
    public const PART_ELECTRONIC = 5;

    public function getLabels(): array
    {
        return [
            self::PART_ICU => 'ДВС',
            self::PART_BODY => 'Кузов',
            self::PART_DRIVETRAIN => 'Трансмиссия',
            self::PART_SUSPENSION => 'Ходовая',
            self::PART_ELECTRONIC => 'Электрика',
        ];
    }

    public function getLabel(string $id): string
    {
        return $this->getLabels()[$id] ?? '';
    }
}