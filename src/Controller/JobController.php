<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\ClientCar;
use App\Entity\Job;
use App\Entity\Part;
use App\Form\ClientCarType;
use App\Form\ClientType;
use App\Form\JobType;
use App\Repository\AppointmentRepository;
use App\Repository\CarRepository;
use App\Repository\ClientCarRepository;
use App\Repository\ClientRepository;
use App\Repository\JobRepository;
use App\Repository\MalfunctionRepository;
use App\Repository\PartRepository;
use App\Repository\ServiceRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Exception\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class JobController extends AbstractController
{

    public function __construct (
        private JobRepository $jobRepository,
        private AppointmentRepository $appointmentRepository,
        private PartRepository $partRepository,
        private ServiceRepository $serviceRepository,
        private MalfunctionRepository $malfunctionRepository,
    )
    {}
    #[Route('/job', name: 'job')]
    public function index(Request $request): Response
    {

        $applointmentList = [];
        foreach ($this->appointmentRepository->findAll() as $appointment) {
            $applointmentList[$appointment->getDate()->format('Y-m-d H:i') . ' - ' . $appointment->getClient()->getName()] = $appointment->getId();
        }

//      оно будет заполняться в js
        $partList = [];
        foreach ($this->partRepository->findAll() as $part) {
            $partList[$part->getSku() . ': ' . $part->getName()] = $part->getId();
        }

        $jobs = $this->jobRepository->findAll();
//        foreach ( as $job) {
//            $jobList
//        }

        $serviceList = [];
        foreach ($this->serviceRepository->findAll() as $service) {
            $serviceList[$service->getName()] = $service->getId();
        }

        $malfunctionList = [];
        foreach ($this->malfunctionRepository->findAll() as $malfunction) {
            $malfunctionList[$malfunction->getName()] = $malfunction->getId();
        }

        $form = $this->createForm(JobType::class, new Job(), [
            'action' => '/add',
            'method' => 'POST'
        ]);


        return $this->render('job/base.html.twig', [
            'form' => $form->createView(),
            'form_label' => 'Работы',
            'appointments' => $applointmentList,
            'services' => $serviceList,
            'malfunction' => $malfunctionList,
            'parts' => $partList
        ]);
    }

    /**
     * @throws ORMException
     */
    #[Route('/job/add', name: 'job.add', methods: ['POST'])]
    public function add(Request $request): Response
    {
        $form = $this->createForm(JobType::class, new Job());

        // handle the request
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
//            $data = $form->getData();




            foreach ($request->request->getIterator() as $i)
            {

                $appointmentId = $i['appointment'] ?? null;
                if (!$appointmentId) break;

                $services = $i['service'] ?? [];
                $parts = $i['part'] ?? [];

                if (!empty($services)) {
                    $this->saveServices($appointmentId, $services);
                }

                if (!empty($parts)) {
                    $this->saveParts($appointmentId, $parts);

                }

            }



        }

        // render the template and pass the form as a variable

        return $this->redirectToRoute('job');
    }

    /**
     * @throws ORMException
     */
    public function saveServices(int $appointmentId, array $data): void
    {
        foreach ($data as $jobData) {
            $job = new Job();
            $job->setAppointmentId($appointmentId);

            if (!empty($serviceId = $jobData['id'])) {
                $service = $this->serviceRepository->find($serviceId);
                $job->setService($service);
            }

            if (!empty($timeTaken = $jobData['timeTakenInMinutes'])) {
                $job->setTimeTakenInMinutes($timeTaken);
            }

            if (!empty($malfunctionId = $jobData['malfunction'])) {
                $malfunction = $this->malfunctionRepository->find($malfunctionId);
                $job->setMalfunction($malfunction);
            }

            $this->jobRepository->save($job, true);
        }
    }

    public function saveParts(int $appointmentId, array $data): void
    {
        if (empty($data)) return;

        $appointment = $this->appointmentRepository->find($appointmentId);

        if (!$appointment) return;

        foreach ($data as $partData) {
            if ($partData == 'default') continue;

            $part = $this->partRepository->find((int)$partData);

            $appointment->addPart($part);

            $this->appointmentRepository->save($appointment, true);

        }


    }
}