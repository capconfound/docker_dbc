<?php

namespace App\Controller\Admin;

use App\Entity\CarPart;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class CarPartCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CarPart::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
