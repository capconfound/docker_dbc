<?php

namespace App\Controller\Admin;

use App\Entity\Appointment;
use App\Entity\Car;
use App\Entity\CarPart;
use App\Entity\Client;
use App\Entity\ClientCar;
use App\Entity\Employee;
use App\Entity\Job;
use App\Entity\JobService;
use App\Entity\Malfunction;
use App\Entity\Part;
use App\Entity\Service;
use App\Entity\Supplier;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
//        return parent::index();

        $routeBuilder = $this->container->get(AdminUrlGenerator::class);
        $url = $routeBuilder->setController(AppointmentCrudController::class)->generateUrl();

        return $this->redirect($url);

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Project');
    }

    public function configureMenuItems(): iterable
    {
//        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
        return [
            MenuItem::linkToDashboard('Dashboard', 'fa fa-home'),

            MenuItem::linkToCrud('Записи на обслуживание', 'fa fa-file-text', Appointment::class),

            MenuItem::section('Client and Car'),
            MenuItem::linkToCrud('Car', 'fa fa-car', Car::class),
            MenuItem::linkToCrud('ClientCar', 'fa fa-user', ClientCar::class),
            MenuItem::linkToCrud('Client', 'fa fa-user', Client::class),

            MenuItem::section('Ремонт'),
            MenuItem::linkToCrud('Неисправности', 'fa fa-wrench', Malfunction::class),
            MenuItem::linkToCrud('Услуги', 'fa fa-file-text', Service::class),


            MenuItem::section('Запчасти'),
            MenuItem::linkToCrud('Поставщики', 'fa fa-truck', Supplier::class),
            MenuItem::linkToCrud('Детали', 'fa fa-cog', Part::class),
            MenuItem::linkToCrud('Связь детали с авто', 'fa fa-file-text', CarPart::class),

            MenuItem::section('Работы'),
            MenuItem::linkToCrud('Работы', 'fa fa-file-text', Job::class),
            MenuItem::linkToCrud('Связь работы с услугой', 'fa fa-file-text', JobService::class),


            MenuItem::section('Персонал'),
            MenuItem::linkToCrud('Персонал', 'fa fa-male', Employee::class),


        ];
    }
}
