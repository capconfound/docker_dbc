<?php

namespace App\Controller\Admin;

use App\Entity\Appointment;
use App\Helper\AppointmentStatusList;
use App\Repository\ClientRepository;
use App\Repository\EmployeeRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class AppointmentCrudController extends AbstractCrudController
{
    public function __construct(
        private ClientRepository $clientRepository,
        private EmployeeRepository $employeeRepository,
        private AppointmentStatusList $statusList,
    )
    {
    }

    public static function getEntityFqcn(): string
    {
        return Appointment::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Запись')
            ->setSearchFields(['client', 'manager', 'date', 'status'])
            ->setDefaultSort(['date' => 'DESC'])
        ;
    }

    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add(EntityFilter::new('client'))
            ->add(EntityFilter::new('manager'))
        ;

    }

//    public function configureFields(string $pageName): iterable
//    {
//        $clients = $this->clientRepository->findAll();
//
//        $clientArr = [];
//
//        foreach ($clients as $client) {
//            $clientArr[$client->getId() . ' ' . $client->getName()] = $client->getId();
//        }
//
//        $employees = $this->employeeRepository->findAll();
//
//        $empArr = [];
//
//        foreach ($employees as $employee) {
//            $empArr[$employee.''] = $employee->getId();
//        }
//
//        yield Field::new('id')->onlyOnIndex();
////        yield ChoiceField::new('clientName')->setChoices($clientArr);
//        yield ChoiceField::new('clientName')->setChoices($clientArr);
////        yield ChoiceField::new('managerName')->setChoices($empArr);
//        yield Field::new('date');
//        yield TextField::new('complaints');
//        yield ChoiceField::new('status')->setChoices($this->statusList->getLabels());
//
////        return [
////            IdField::new('id'),
////            TextField::new('title'),
////            TextEditorField::new('description'),
////        ];
//    }

}
