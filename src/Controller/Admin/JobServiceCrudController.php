<?php

namespace App\Controller\Admin;

use App\Entity\JobService;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class JobServiceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return JobService::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
