<?php

namespace App\Controller\Admin;

use App\Entity\ClientCar;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ClientCarCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return ClientCar::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
