<?php

namespace App\Controller;

use App\Entity\Appointment;
use App\Form\AppointmentType;
use App\Helper\AppointmentStatusList;
use App\Repository\AppointmentRepository;
use App\Repository\ClientCarRepository;
use App\Repository\ClientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AppointmentController extends AbstractController 
{
    public function __construct(
        private AppointmentRepository $appointmentRepo,
        private ClientRepository $clientRepository,
        private ClientCarRepository $clientCarRepository,
        private AppointmentStatusList $statusList,
    )
    {}

    #[Route('/appointment', name: 'appointment')]
    public function index(Request $request): Response
    {

        $appointments = $this->getAppointmentList();

        $form = $this->createForm(AppointmentType::class, new Appointment(), [
            'action' => '/add',
            'method' => 'POST'
        ]);



        return $this->render('appointment/base.html.twig', [
            'form' => $form->createView(),
            'form_label' => 'Создание новой записи',
            'appointments' => $appointments,
            'status_labels' => $this->statusList->getLabels()
        ]);
    }

    #[Route('/appointment/add', name: 'appointment.add', methods: ['POST'])]
    public function add(Request $request): Response
    {
        $form = $this->createForm(AppointmentType::class, new Appointment(), [
            'action' => 'add',
            'method' => 'POST',
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() ) {
            $data = $form->getData();



            $clientCarId = null;
            foreach ($request->request->getIterator() as $i)
            {
                foreach ($i as $key => $value) {
                    if ($key == 'clientCar') {
                        $clientCarId = $value;
                        break;
                    }
                }
            }

            $clientCar = $this->clientCarRepository->findOneBy(['id' => $clientCarId]);

            if (!empty($clientCar)) {
                $data->setClientCar($clientCar);
            }

            $data->setStatus(AppointmentStatusList::SCHEDULED);

            $this->appointmentRepo->save($data, true);
        }

        return $this->redirectToRoute('appointment');
    }

    #[Route('/appointment/{id}/edit', name: 'appointment.edit',)]
    public function edit(Request $request, int $id): Response
    {

        return $this->render('appointment/edit.html.twig', [
            'id' => $id
        ]);
    }

    #[Route('/appointment/{id}/delete', name: 'appointment.delete',)]
    public function delete(Request $request, int $id): Response
    {
        // оно ничего не делает. надо имплементировать метод строчкой ниже.
        $this->appointmentRepo->deleteById($id);

        return $this->redirectToRoute('appointment');
    }

    public function getAppointmentList(): array
    {
        $appointmentList = $this->appointmentRepo->findNotDeleted();

        usort($appointmentList, function($a, $b) {
            return $a->getDate()->format('U') - $b->getDate()->format('U');
        });

        return $appointmentList;
    }
}