<?php

namespace App\Controller;

use App\Repository\CarRepository;
use App\Repository\ClientCarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CarController extends AbstractController
{

    public function __construct (
        private ClientCarRepository $clientCarRepository,
        private CarRepository $carRepository,
    )
    {}

    #[Route('/car/get', name: 'car.get', methods: ['GET'])]
    public function getCarsForClientId(Request $request): JsonResponse
    {
        $clientId = $request->query->get('client_id');

        // Retrieve the list of car IDs based on the client ID from the repository
        $carIdList = $this->clientCarRepository->getCarIdsByClientId($clientId);

        if (empty($carIdList)) return $this->json([]);

        $carList = [];
        foreach ($carIdList as $car) {
            $carId = $car['id'];
            $carName = $this->carRepository->findNameById($carId)[0][1];
            $carPlate = $this->clientCarRepository->findOneBy(['id' => $carId])->getPlate();

            $carList[] = [
                'id' => $carId,
                'name' => $carPlate . ': ' . $carName
            ];
        }



        return $this->json(['car_choices' => $carList]);
    }

}