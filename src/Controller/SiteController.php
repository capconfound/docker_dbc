<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class SiteController extends AbstractController
{

    public function __construct(
    )
    {
    }

    #[Route('/')]
    public function index(): Response
    {

        return $this->render('base.html.twig');
    }
}