<?php

namespace App\Controller;

use App\Repository\CarRepository;
use App\Repository\ClientCarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReportController extends AbstractController
{

    public function __construct (
    )
    {}

    #[Route('/report', name: 'report')]
    public function index(): Response
    {


        return $this->render('report/base.html.twig');
    }

}