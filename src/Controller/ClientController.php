<?php

namespace App\Controller;

use App\Entity\Client;
use App\Entity\ClientCar;
use App\Form\ClientCarType;
use App\Form\ClientType;
use App\Repository\CarRepository;
use App\Repository\ClientCarRepository;
use App\Repository\ClientRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ClientController extends AbstractController
{

    public function __construct (
        private ClientRepository $clientRepository,
        private CarRepository $carRepository,
        private ClientCarRepository $clientCarRepository,
    )
    {}
    #[Route('/client', name: 'client')]
    public function index(Request $request): Response
    {
        $clients = $this->clientRepository->findActive();
        $cars = $this->carRepository->findAll();
        $carList = [];
        foreach ($cars as $car) {
            $carList[$car->getName()] = $car->getId();
        }

        $clientsList = [];
        foreach ($clients as $client) {
            $clientsList[$client->getName()] = $client->getId();
        }


        $form = $this->createForm(ClientType::class, new Client(), [
            'action' => '/add',
            'method' => 'POST'
        ]);

        $formCars = $this->createForm(ClientCarType::class, new ClientCar(), [
            'action' => '/add',
            'method' => 'POST'
        ]);

        return $this->render('client/base.html.twig', [
            'form' => $form->createView(),
            'form1' => $formCars->createView(),
            'clients' => $clients, // objects
            'client_list' => $clientsList, // for select input
            'cars' => $carList,
            'form1_label' => 'Новый клиент',
            'form2_label' => 'Связка клиента и автомобиля',
        ]);
    }

    #[Route('/client/add', name: 'client.add', methods: ['POST'])]
    public function add(Request $request): Response
    {
        $client = new Client();
        $form = $this->createForm(ClientType::class, $client);

        // handle the request
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // do something with the client object, e.g. save it to database
            $data = $form->getData();


            $this->clientRepository->save($data, true);
            // redirect to another page or show a success message
        }

        // render the template and pass the form as a variable

        return $this->redirectToRoute('client');
    }

    #[Route('/client/add/car', name: 'client.add.car', methods: ['POST'])]
    public function addCar(Request $request): Response
    {

        $form = $this->createForm(ClientCarType::class, new ClientCar());

        // handle the request
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            // do something with the client object, e.g. save it to database
            $data = $form->getData();


//            print_r([
//                $data->getCar()->getName(),
//                $data->getClient()->getId(),
//            ]); die;

            $this->clientCarRepository->save($data, true);
            // redirect to another page or show a success message
        }

        // render the template and pass the form as a variable

        return $this->redirectToRoute('client');
    }

    #[Route('/client/{id}/delete', name: 'client.delete',)]
    public function delete(Request $request, int $id): Response
    {
        // оно ничего не делает. надо имплементировать метод строчкой ниже.
        $this->clientRepository->deleteById($id);

        return $this->redirectToRoute('client');
    }


}